// app/models/vehicle.js

var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var ServicehistorySchema = new Schema({
    miles: Number,
    date: Date,
    cost: Number,
    name: { type: String, default: '' },
    memo: String,
    _category: { type: mongoose.Schema.Types.ObjectId, ref: 'Category' },
    _vehicle: { type: mongoose.Schema.Types.ObjectId, ref: 'Vehicle' }
});

module.exports = mongoose.model('Servicehistory', ServicehistorySchema);