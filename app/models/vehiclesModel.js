// app/models/vehicle.js

var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var VehicleSchema = new Schema({
    name:   String,
    year:   Number,
    make:   { type: String, default: '' },
    model:  { type: String, default: '' },
    _user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
});

module.exports = mongoose.model('Vehicle', VehicleSchema);