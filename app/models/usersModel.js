// app/models/user.js

var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var UserSchema = new Schema({
   email: String,
   username: String,
   hashed_password: { type: String, select: false },
   salt: String
});

module.exports = mongoose.model('User', UserSchema);