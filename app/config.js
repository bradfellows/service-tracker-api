module.exports = {
    port:       process.env.PORT,
    ip:         process.env.IP,
    secret:     'mysecret',
    database:   process.env.IP+'/api-tracker-dev'
};