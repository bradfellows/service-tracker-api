var Vehicle = require('../models/vehiclesModel');

module.exports = {
    
    create: function(req,res){
        
        var vehicle = new Vehicle(); // assign new vehicle instance
        vehicle.name = req.body.name;
        vehicle.make = req.body.make;
        vehicle.model = req.body.model;
        vehicle.year = req.body.year;
        vehicle._user = req.serviceApp.token._id;
        
        vehicle.save(function(err){
            if(err){ res.send(err); }
            res.json({  message: 'vehicle created' });
        });
    },

    getAll: function(req,res){
        console.log('vehiclesController.getAll')
        console.log(req.serviceApp);
        Vehicle.find({
            _user: req.serviceApp.token._id
        },function(err,vehicles){
            if(err){ res.send(err); }
            res.json(vehicles);
        });
      
    },

    getOne: function(req,res){
        Vehicle.findById(req.params.vehicle_id, function(err,vehicle){
            if(err){ res.send(err); }
            res.json(vehicle);
        });
    },

    update: function(req,res){
      
        // use Vehicle model to find it
        Vehicle.findById(req.params.vehicle_id, function(err, vehicle){
            if(err){ res.send(err); }
            
            vehicle.name = req.body.name; // update vehicle info
            vehicle.make = req.body.make;
            vehicle.model = req.body.model;
            
            // save vehicle
            vehicle.save(function(err){
                if(err){ res.send(err); }
                res.json({  message: 'vehicle updated' });
            });
        });
        
    },

    delete: function(req,res){
        Vehicle.remove({
            _id: req.params.vehicle_id
        }, function(err,vehicle){
            if(err){ res.send(err); }
            res.json({  message:'delete successful'   });
        });
    }
};