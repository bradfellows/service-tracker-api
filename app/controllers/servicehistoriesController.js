var Servicehistory = require('../models/servicehistoriesModel');

module.exports = {
    
    create: function(req,res){
        
        var servicehistory = new Servicehistory();
        servicehistory.miles = req.body.miles;
        servicehistory.date = Date.now();
        servicehistory.cost = req.body.cost;
        servicehistory.name = req.body.name;
        servicehistory.memo = req.body.memo;
        servicehistory._category = req.body._category;
        servicehistory._vehicle = req.body._vehicle;
        
        servicehistory.save(function(err){
            if(err){ res.send(err); }
            res.json({ message: 'service history created '});
        });
    },
    
    getAll: function(req,res){
        
        Servicehistory.find(function(err,servicehistories){
            if(err){ res.send(err); }
            res.json(servicehistories);
        });
        
    },
    
    getOne: function(req,res){
        Servicehistory.findById(req.params.servicehistory_id, function(err, servicehistory){
           if(err){ res.send(err); }
           res.json(servicehistory);
        });
    },
    
    update: function(req,res){
        Servicehistory.findById(req.params.servicehistory_id, function(err, servicehistory){
            if(err){ res.send(err); }
            
            servicehistory.miles = req.body.miles;
            servicehistory.date = req.body.date;
            servicehistory.cost = req.body.cost;
            servicehistory.name = req.body.name;
            servicehistory.memo = req.body.memo;
            servicehistory._category = req.body._category; // update service history info from request body
            
            servicehistory.save(function(err){
               if(err){ res.send(err); }
               res.json({ message: 'service history updated'});
            });
        });
    },
    
    remove: function(req,res){
        Servicehistory.remove({
            _id: req.params.servicehistory_id
        }, function(err, servicehistory){
           if(err){ res.send(err); }
           res.json({ message: 'delete successful ' });
        });
    },
    
    // this can be a lot more modular
    // do a generic find that responds to a POST with the body content specifying criteria
    findByVehicle: function(req,res){
        console.log(req.decoded);
        Servicehistory.find({ _vehicle: req.params.vehicle_id },function(err,servicehistories){
            if(err){ res.send(err); }
            res.json(servicehistories);
        });
    }
    
};