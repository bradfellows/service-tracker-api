var jwt = require('jsonwebtoken');
var User = require('../models/usersModel');

module.exports = {
    
    authenticate: function(req,res){
        
        User.findOne({
            username: req.body.username
        }, '+hashed_password', function(err,user){
            
            console.log('authenticating... ');
            
            if(err) throw err;
            
            if(!user){
                console.log('user fail');
                res.status(403).send({
                    success: false,
                    message: 'Authentication failed.'
                });
            } else if(user){
                
                if(user.hashed_password != req.body.password){
                    console.log('password fail');
                    res.status(403).send({
                        success: false,
                        message: 'Authentication failed.'
                    });
                } else {
                    // user found, password correct
                    // create token
                    
                    user = user.toObject();
                    delete user.hashed_password;
                    delete user.salt;
                    
                    //console.log(user);
                    
                    var token = jwt.sign(user, 'secret', {
                        expiresIn: 30*24*60*60
                    });
                    
                    res.json({
                        success: true,
                        message: 'Authentication successful.',
                        token: token
                    });
                }
            }
        });
        
    },
    
    signup: function(req,res){
        //console.log(req.body);
        
        var user = new User();
        user.email = req.body.email;
        user.username = req.body.username;
        user.hashed_password = req.body.password;
        user.salt = 'this password is not secure';
        
        user.save(function(err){
            if(err) { res.send(err); }
            res.json({message: 'sign up success'});
        });
    },
    
    checkAuth: function(req,res){
        // THIS IS SUPER KLUDGE, THERE HAS TO BE A WAY TO CHECK THE TOKEN w/ MODULAR CODE
        // PROBABLY HAVE TO REQUIRE THIS CONTROLLER IN THE verifyToken MIDDLEWARE
        
        var token = req.headers['x-access-token'];
        
        if(token){
            console.log("got token?");
            jwt.verify(token, 'secret', function(err, decoded){
                //console.log(decoded);
               if(err) res.send({auth:false});
               else res.send({auth:true});
            });
        } else res.send({auth:false});
    }
};