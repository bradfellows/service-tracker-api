var User = require('../models/usersModel');

module.exports = {
  
  create: function(req,res){
      var user = new User();
      user.username = req.body.username;
      user.hashed_password = req.body.password;
      user.salt = 'salt';
      
      user.save(function(err){
          if(err){res.send(err);}
          res.json({message:'user created'});
      });
  },
  
  getAll: function(req,res){
      User.find(function(err,users){
          if(err){ res.send(err); }
          res.json(users);
      });
  },
  
  getOne: function(req,res){
      User.findById(req.params.user_id, function(err, user){
          if(err){ res.send(err); }
          res.json(user);
      });
  },
  
  update: function(req,res){
      User.findById(req.params.user_id, function(err, user){
          if(err){res.send(err);}
          
          user.username = req.body.username;
          user.hashed_password = req.body.password;
          user.salt = 'salty';
          
          user.save(function(err){
              if(err){res.send(err);}
              res.json({message: 'user updated'});
          });
          
      });
  },
  
  remove: function(req,res){
      User.remove({
          _id: req.params.user_id
      }, function(err, user){
          if(err){res.send(err);}
          res.json({message: 'update successful'});
      });
  }
    
};