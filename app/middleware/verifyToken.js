//var express = require('express');
var jwt = require('jsonwebtoken');

module.exports = function(req,res,next) {
    console.log('verifyToken fires');
    
    // check in header or url params or post params for token
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    
    if(token){
        // use decoder ring on token
        jwt.verify(token, 'secret', function(err, decoded){
           if(err){
               res.status(403).send({success: false, message: 'Failed to authenticate token'});
           } else {
               req.serviceApp = { token: decoded} ;
               next(); // <------------ IF EVERYTHING WENT OK GO TO THE NEXT IN THE CHAIN
           }
        });
    } else {
        // if no token return error
        return res.status(403).send({
            success: false,
            message: 'No token provided'
        });
    }
    
};