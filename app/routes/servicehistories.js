// app/routes/servicehistories.js

var express = require('express');
var servicehistories = require('../controllers/servicehistoriesController');

var app = express();
module.exports = app;

// routes ending in /
app.route('/')
    .post(servicehistories.create)
    .get(servicehistories.getAll);
    
// routes ending in /:servicehistory_id
app.route('/:servicehistory_id')
    .get(servicehistories.getOne)
    .put(servicehistories.update)
    .delete(servicehistories.remove);