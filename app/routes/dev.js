// app/routes/servicehistories.js

var express = require('express');
var User = require('../models/usersModel');

var app = express();
module.exports = app;

// routes ending in /
app.route('/addTestUser')
    .get(function(req,res,next){
        
        var brad = new User({
            username: 'brad f',
            hashed_password: 'apathy',
            salt: 'salt'
        });
        
        brad.save(function(err){
            if(err) throw err;
            console.log('user saved');
            res.json({success:true});
        });
    });