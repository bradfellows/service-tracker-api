// app/routes/users.js

var express = require('express');
var usersController = require('../controllers/usersController');

var app = express();
module.exports = app;

// routes ending in /
app.route('/')
    .post(usersController.create)
    .get(usersController.getAll);
    
// routes ending in /:user_id
app.route('/:user_id')
    .get(usersController.getOne)
    .put(usersController.update)
    .delete(usersController.remove);