// app/routes/auth.js

var express = require('express');
var authController = require('../controllers/authController');

var app = express();
module.exports = app;

app.route('/')
    .get(authController.checkAuth)
    .post(authController.authenticate)
    ;
    
app.route('/signup')
    .post(authController.signup);