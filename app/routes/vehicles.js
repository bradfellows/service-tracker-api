// app/routes/vehicles.js

var express = require('express');
var vehiclesController = require('../controllers/vehiclesController');
var servicehistoriesController = require('../controllers/servicehistoriesController');

// var app = module.exports = express();
var app = express();
module.exports = app;

// routes ending in /
app.route('/')
   .post(vehiclesController.create)
   .get(vehiclesController.getAll);
   
// routes ending in /:vehicle_id
app.route('/:vehicle_id')
   .get(vehiclesController.getOne)
   .put(vehiclesController.update)
   .delete(vehiclesController.delete);
   
app.route('/:vehicle_id/servicehistories')
   .get(servicehistoriesController.findByVehicle);