// server.js

// BASE SETUP
// =============================================================================

// need these packages
var express     = require('express');
var app         = express();
var bodyParser  = require('body-parser');
var mongoose    = require('mongoose');
var cors        = require('cors');

//var jwt = require('jsonwebtoken');

// set environment variables
var config = require('./app/config');

// configure mongoose
mongoose.connect(config.ip+'/api-tracker-dev');

// configure bodyParser
//  to let us get data from POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// =============================================================================
// BASE SETUP \

// CORS MIDDLEWARE
// =============================================================================

// bro, we can't let just anybody access this API... or can we?
/*var allowCORS = require('./app/middleware/allowCORS');
app.use(allowCORS);*/
app.use(cors());

// =============================================================================
// CORS MIDDLEWARE\

// PUBLIC ROUTES
// =============================================================================

var authRoutes = require('./app/routes/auth');
app.use('/auth',authRoutes);

// =============================================================================
// PUBLIC ROUTES \

// TOKEN VALIDATION MIDDLEWARE
// =============================================================================

//var verifyToken = ;
app.use(require('./app/middleware/verifyToken'));

// =============================================================================
// TOKEN VALIDATION MIDDLEWARE \

// SECURED ROUTES
// =============================================================================

// TODO: change routes to how they do it here http://bigspaceship.github.io/blog/2014/05/14/how-to-create-a-rest-api-with-node-dot-js/

var vehicleRoutes = require('./app/routes/vehicles'); // load vehicle routes
app.use('/api/vehicles',vehicleRoutes); //register vehicle routes

var servicehistoryRoutes = require('./app/routes/servicehistories');
app.use('/api/servicehistories', servicehistoryRoutes);

var userRoutes = require('./app/routes/users');
app.use('/api/users', userRoutes);

//var devRoutes = require('./app/routes/dev');
//app.use('/dev', devRoutes);



// =============================================================================
// SECURED ROUTES \



// START SERVER
// =============================================================================
app.listen(config.port);
console.log('listening on port '+config.port);