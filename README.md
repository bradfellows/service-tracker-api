API for vehicle service tracking app.

## Running the server

1) start mongo by running the database launcher (script that launches mongod with the correct paramteres) from the terminal:

    $ ./mongod

2) start the server using node or nodemon

    $ node server.js
    
    -- or --
    
    $ nodemon server.js